package com.duoc.isabelazocar_pruebafinal.Constant;

/**
 * Created by DUOC on 08-07-2017.
 */

public class Constants {

    static public final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    static public final int API_CONNECTION_TIMEOUT = 10;
    public interface APIConstants {
        String CANCHA_LIST = "bins/1487av";
    }

}
