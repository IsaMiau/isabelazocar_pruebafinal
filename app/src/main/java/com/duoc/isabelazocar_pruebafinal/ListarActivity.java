package com.duoc.isabelazocar_pruebafinal;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.duoc.isabelazocar_pruebafinal.Interface.CanchaInterface;
import com.duoc.isabelazocar_pruebafinal.Modelo.Cancha;
import com.duoc.isabelazocar_pruebafinal.Modelo.CanchasList;
import com.duoc.isabelazocar_pruebafinal.RecyclerView.MyAdaptador;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListarActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView rvCanchas;
    private ArrayList<Cancha> canchas = new ArrayList<>();
    private Context c;
    private MyAdaptador adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Examen Android");
        setSupportActionBar(toolbar);

        rvCanchas = (RecyclerView) findViewById(R.id.rvCanchas);
        rvCanchas.hasFixedSize();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(c);
        rvCanchas.setLayoutManager(layoutManager);

        listarCanchas();

    }

    private void listarCanchas() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        final CanchaInterface service = retrofit.create(CanchaInterface.class);
        final Call<CanchasList> repos = service.getListCanchas();
        repos.enqueue(new Callback<CanchasList>() {

            @Override
            public void onResponse(Call<CanchasList> call, Response<CanchasList> response) {

                if (response.code() == 200) {
                    CanchasList values = response.body();
                    canchas = new ArrayList<>(values.getList());
                    adapter = new MyAdaptador(getApplicationContext(),canchas);
                    rvCanchas.setAdapter(adapter);


                }
                else{
                    String codigo = response.message();
                    Toast.makeText(c, codigo , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CanchasList> call, Throwable t) {
                Toast.makeText(c, "error", Toast.LENGTH_SHORT).show();
            }


        });

    }

}

