package com.duoc.isabelazocar_pruebafinal.Modelo;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DUOC on 08-07-2017.
 */

public class CanchasList {

    @SerializedName("canchas")
    List<Cancha> list;

    public CanchasList(List<Cancha> list) {
        this.list = list;
    }

    public List<Cancha> getList() {
        return list;
    }

    public void setList(List<Cancha> list) {
        this.list = list;
    }

}
