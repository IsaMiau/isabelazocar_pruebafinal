package com.duoc.isabelazocar_pruebafinal.Modelo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DUOC on 08-07-2017.
 */

public class Cancha {

    @SerializedName("id")
    private int id;
    @SerializedName("nombre_recinto")
    private String nombre_recinto;
    @SerializedName("direccion")
    private String direccion;
    @SerializedName("tipo_cancha")
    private String tipo_cancha;
    @SerializedName("descripcion")
    private String descripcion;
    @SerializedName("cantidad_canchas")
    private int cantidad_canchas;
    @SerializedName("telefono")
    private String telefono;
    @SerializedName("url_sitio")
    private String url_sitio;
    @SerializedName("url_avatar")
    private String url_avatar;
    @SerializedName("url_banner")
    private String url_banner;


    public Cancha() {
    }

    public Cancha(String url_banner, String url_avatar, int id, String nombre_recinto, String direccion, String tipo_cancha, String descripcion, int cantidad_canchas, String telefono, String url_sitio) {
        this.url_banner = url_banner;
        this.url_avatar = url_avatar;
        this.id = id;
        this.nombre_recinto = nombre_recinto;
        this.direccion = direccion;
        this.tipo_cancha = tipo_cancha;
        this.descripcion = descripcion;
        this.cantidad_canchas = cantidad_canchas;
        this.telefono = telefono;
        this.url_sitio = url_sitio;
    }

    public String getUrl_banner() {
        return url_banner;
    }

    public String getUrl_avatar() {
        return url_avatar;
    }

    public int getId() {
        return id;
    }

    public String getNombre_recinto() {
        return nombre_recinto;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTipo_cancha() {
        return tipo_cancha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getCantidad_canchas() {
        return cantidad_canchas;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getUrl_sitio() {
        return url_sitio;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre_recinto(String nombre_recinto) {
        this.nombre_recinto = nombre_recinto;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTipo_cancha(String tipo_cancha) {
        this.tipo_cancha = tipo_cancha;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCantidad_canchas(int cantidad_canchas) {
        this.cantidad_canchas = cantidad_canchas;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setUrl_sitio(String url_sitio) {
        this.url_sitio = url_sitio;
    }

    public void setUrl_avatar(String url_avatar) {
        this.url_avatar = url_avatar;
    }

    public void setUrl_banner(String url_banner) {
        this.url_banner = url_banner;
    }
}
