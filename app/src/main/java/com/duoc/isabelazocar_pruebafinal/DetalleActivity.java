package com.duoc.isabelazocar_pruebafinal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetalleActivity extends AppCompatActivity {

    private TextView tvNombre, tvDesc;
    private ImageView ivImagenCancha;
    private Button btnWeb, btnTelefono;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Examen Android");
        setSupportActionBar(toolbar);

        tvNombre = (TextView) findViewById(R.id.tvNombre);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
        ivImagenCancha = (ImageView) findViewById(R.id.ivImagenCancha);
        btnWeb = (Button) findViewById(R.id.btnWeb);
        btnTelefono = (Button) findViewById(R.id.btnTelefono);

        tvNombre.setText(getIntent().getExtras().getString("nombre"));
        tvDesc.setText(getIntent().getExtras().getString("descripcion"));
        Picasso.with(this).load(getIntent().getExtras().getString("banner")).placeholder(ivImagenCancha.getDrawable()).fit().into(ivImagenCancha);

        final String url = getIntent().getExtras().getString("web");
        final String telefono = getIntent().getExtras().getString("telefono");

        btnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });

        btnTelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel: "+telefono));
                startActivity(intent);
            }
        });

    }
}
