package com.duoc.isabelazocar_pruebafinal.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.duoc.isabelazocar_pruebafinal.DetalleActivity;
import com.duoc.isabelazocar_pruebafinal.Modelo.Cancha;
import com.duoc.isabelazocar_pruebafinal.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DUOC on 08-07-2017.
 */

public class MyAdaptador extends RecyclerView.Adapter<MyAdaptador.MyViewHolder>{

    private Context context;
    private ArrayList<Cancha> canchas;


    public MyAdaptador(Context context, ArrayList<Cancha> canchas) {
        this.context = context;
        this.canchas = canchas;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.items_canchas, parent, false);

        MyViewHolder holder = new MyViewHolder(parent.getContext(), convertView, canchas);

        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Cancha can = canchas.get(position);
        Picasso.with(context).load(can.getUrl_avatar()+"").placeholder(holder.imgCancha.getDrawable()).fit().into(holder.imgCancha);
        holder.tvNombre.setText(canchas.get(position).getNombre_recinto()+"");
        holder.tvDireccion.setText(canchas.get(position).getDireccion()+"");
        holder.tvTipo.setText(canchas.get(position).getTipo_cancha()+"");
        holder.tvCantidad.setText(canchas.get(position).getCantidad_canchas()+"");
    }

    @Override
    public int getItemCount() {
        return this.canchas.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvNombre, tvDireccion, tvTipo, tvCantidad;
        ImageView imgCancha;
        ArrayList<Cancha> canchas = new ArrayList<Cancha>();
        Context context;

        public MyViewHolder(Context context, View itemView, ArrayList<Cancha> canchas) {
            super(itemView);
            this.canchas = canchas;
            this.context = context;

            itemView.setOnClickListener(this);
            imgCancha = (ImageView) itemView.findViewById(R.id.imgCancha);
            tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
            tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
            tvTipo = (TextView) itemView.findViewById(R.id.tvtipo);
            tvCantidad = (TextView) itemView.findViewById(R.id.tvCantidad);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Cancha can = this.canchas.get(position);
            Intent intent = new Intent(context, DetalleActivity.class);
            intent.putExtra("banner", can.getUrl_banner());
            intent.putExtra("nombre", can.getNombre_recinto());
            intent.putExtra("descripcion", can.getDescripcion());
            intent.putExtra("web", can.getUrl_sitio());
            intent.putExtra("telefono", can.getTelefono());

            this.context.startActivity(intent);
        }
    }

}
