package com.duoc.isabelazocar_pruebafinal.Interface;

import com.duoc.isabelazocar_pruebafinal.Modelo.CanchasList;

import retrofit2.Call;
import retrofit2.http.GET;

import static com.duoc.isabelazocar_pruebafinal.Constant.Constants.APIConstants.CANCHA_LIST;

/**
 * Created by DUOC on 08-07-2017.
 */

public interface CanchaInterface {
    @GET(CANCHA_LIST)
    Call<CanchasList> getListCanchas();
}
